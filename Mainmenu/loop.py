#!/usr/bin/env python3
# -*- coding: utf8 -*-

#Copyright © 2020 Benoît Boudaud <http://miamondo.org>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>

#==== ABOUT THIS SCRIPT ========================================================

"""This program instantiates a loop, and is part of a personal project named
'Labortablo'. This project aims to create a desktop environment
entirely written in Python and using the GUI Tkinter."""

#==== PREREQUISITES ============================================================

# see https://gitlab.com/miamondo/labortablo/-/blob/master/README.md

#======= MODULES ===============================================================

import os
import sys
import subprocess
import time

#==== CLASS ====================================================================

class Loop(object):
    "launches the loop"

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"
        
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def loop(self):
        "launches the loop"

        i = 0
        while i < 2:
            
            running_apps = list()

            process = subprocess.Popen(
                ['wmctrl', '-l', '-p'],
                text = True,
                stdout = subprocess.PIPE,
                stderr = subprocess.STDOUT)

            wmctrl = process.stdout.readlines()

            for line in wmctrl:
                running_apps.append([
                    line.split()[0],
                    line.split()[2],
                    ' '.join(line.split()[4:])])
            with open(os.path.join(self.cwd, 'wmctrl.txt'), 'w') as wfile:
                for app in running_apps:
                    wfile.write('{}:{}:{}\n'.format(app[0], app[1], app[2]))
            i+=1
            time.sleep(.5)
            if i==1:
                i=0

#==== MAIN PROGRAMM ============================================================

if __name__ == '__main__':
    "This file launches a loop that checks the running applications"

    init = {
        'cwd': os.path.abspath(os.path.dirname(sys.argv[0]))
        }

    loop = Loop(init)
    loop.loop()
