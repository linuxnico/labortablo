#!/usr/bin/env python3
# -*- coding: utf8 -*-

#Copyright © 2020 Benoît Boudaud <http://miamondo.org>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>

#==== ABOUT THIS SCRIPT ========================================================

"""This program instantiates a menu of applications and a wallpaper. It is 
included in a personal project named 'Labortablo'. This project aims to create 
a desktop environment entirely written in Python and using the GUI Tkinter."""

#==== PREREQUISITES ============================================================

# see https://gitlab.com/miamondo/labortablo/-/blob/master/README.md

#======= MODULES ===============================================================

import os
import sys
import subprocess
import shutil
import tkinter as tk
import tkinter.messagebox as tkm

from PIL import Image, ImageTk

#==== CLASS ====================================================================

class MainMenu(object):
    "this file launches the main menu, the submenus and the wallpaper(s)"

    floatingMenu = False 
    # If value is "False", Menu of categories is opened by pressing the button
    # If value is "True", it is opened by clicking on the desktop

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def mainMenu(self):
        "This method instantiates the wallpaper, the main menu and the submenus"

        applications = os.listdir('/usr/share/applications')
        try:
            with open(os.path.join(self.cwd, 'usr_share_applications'), 'r') as rfile:
                if rfile.readlines() != applications:
                    shutil.rmtree(os.path.join(self.cwd, 'Images', 'Icons'))
            with open(os.path.join(self.cwd, 'usr_share_applications'), 'w') as wfile:
                for app in applications:
                    wfile.write(app + '\n')
        except:
            with open(os.path.join(self.cwd, 'usr_share_applications'), 'w') as wfile:
                for app in applications:
                    wfile.write(app + '\n')

        #We create a margin of 35px for the taskbar at the bottom of the screen.
        if not 'margins' in os.listdir(self.cwd):
            with open(os.path.join('/home/{}/.config/openbox/rc.xml'.format(
                os.environ['USER'])), 'r') as rfile:
                rcxml = rfile.readlines()
                for i, line in enumerate(rcxml):
                    if line == '    <bottom>0</bottom>\n':
                        rcxml[i:i+1] = ['    <bottom>31</bottom>\n']
                        break
            
            with open(os.path.join('/home/{}/.config/openbox/rc.xml'.format(
                os.environ['USER'])), 'w') as wfile:
                for line in rcxml:
                    wfile.write(line)

        # A small button that iconifies or withdraws the main menu
        self.root = tk.Tk()
        self.root.config(bg = 'black')
        self.root.geometry(
            '+{}-{}'.format(
                self.screenXY.replace('+', ' ').replace('-', ' ').split()[0],
                self.screenXY.replace('+', ' ').replace('-', ' ').split()[1]))
        self.root.overrideredirect(1) # removes the border around the window

        # Image for the menu button. I chose an icon of Archlinux.
        # It should be selected by the user himself.
        self.menuIcon = Image.open(
            os.path.join(
                self.cwd,
                'Images',
                'main_menu.png'))

        self.menuIcon.thumbnail((25, 25))
        self.menuIcon = ImageTk.PhotoImage(
            self.menuIcon, 
            master = self.root)

        # Instantiation of the menu button
        self.menuButton = tk.Button(
            self.root,
            bg = 'black',
            bd = 1,
            image = self.menuIcon,
            highlightbackground = 'navajowhite',
            highlightthickness = 1,
            cursor = 'hand2',
            command = self.deiconifyMenu)
        self.menuButton.pack(fill='both', side='left')

        # Toplevel window for the menu, containing all the category buttons
        self.menuToplevel = tk.Toplevel(self.root)
        self.menuToplevel.config(bg = 'black')
        self.menuToplevel.overrideredirect(1)
        self.withdrawMenu() # At login, automatic withdraw of the main menu

        # A LabelFrame widget showing the name of the user
        self.menuLabelFrame = tk.LabelFrame(
            self.menuToplevel,
            fg = 'navajowhite',
            text = ' {} '.format(os.environ['USER']),
            bg = 'black',
            bd = 2,
            font = 'Courier 12 bold',
            highlightcolor = 'navajowhite',
            highlightbackground = 'navajowhite')
        self.menuLabelFrame.pack(padx=10, pady=10)

        # Wallpaper
        self.wallpaperToplevel = tk.Toplevel(self.root)
        self.wallpaperToplevel.config(bg = 'black')
        self.wallpaperToplevel.geometry(
            '{}x{}{}'.format(
                self.screenWidth,
                self.screenHeight,
                self.screenXY))
        self.wallpaperToplevel.overrideredirect(1)
        self.wallpaperToplevel.lower()

        # Wallpaper image is processed and Label widget is instantiated
        img = self.wallpaper

        self.wpIcon = Image.open(
            os.path.join(
                self.cwd,
                'Images',
                img))

        self.wpIcon = ImageTk.PhotoImage(
            self.wpIcon,
            master = self.wallpaperToplevel)

        self.wallpaperLabel = tk.Label(
            self.wallpaperToplevel,
            bg = 'black',
            image = self.wpIcon,
            width = self.screenWidth,
            height=self.screenHeight)
        self.wallpaperLabel.pack()

        self.wallpaperLabel.bind('<Button-1>', self.withdrawMenu)
        self.wallpaperLabel.bind('<Button-3>', self.deiconifyMenu)

        applications = '/usr/share/applications'

        # All the png icons in /usr/share are gathered here, in this dictionary:
        icons = dict()
                # minimum size is 48x48 (for a better resolution)
        for root, dirs, files in os.walk('/usr/share'):
            for f in files:
                if os.path.splitext(f)[1]==('.png'):
                    try: # only PNG files
                        img = Image.open(os.path.join(root, f))
                        if img.width >= 48 and img.height >= 48: 
                            icons[os.path.splitext(f)[0]] = os.path.join(root, f)
                    except:
                        pass
#       The menu is divided in 12 categories
        categories = [
            ['Applications', ' Applications', 'computer-laptop.png'],
            ['Directories', ' Gestionnaire de fichiers', 'thunar.png'],
            ['WebBrowser', ' Internet', 'applications-internet.png'],
            ['Utility', ' Accessoires', 'applications-accessories.png'],
            ['Office', ' Bureautique', 'applications-office.png'],
            ['Settings', ' Réglages', 'preferences-desktop.png'],
            ['Development', ' Developpement', 'applications-development.png'],
            ['Education', ' Éducation', 'applications-education.png'],
            ['Graphics', ' Graphisme', 'applications-graphics.png'],
            ['AudioVideo', ' Multimédia', 'applications-multimedia.png'],
            ['Network', ' Réseau', 'network-wired.png'],
            ['System', ' Système', 'applications-system.png']]

        # In the following dictionary, the program gathers the name, the 
        # command, the icon and the category of each application installed on 
        #the computer.
        # This part is quite difficult to understand. It starts with the name
        NEIC = dict() # NEIC means Names, Execs, Icons, Categories

        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Name='):
                            f = f.replace('.desktop','')
                            NEIC[f] = ['name:' + l.split('=')[1].strip()]
                            break #Result is {chromium:['name:Chromium']}
                    except:
                        pass

        #Let's continue with the command (exec)
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Exec='):
                            f = f.replace('.desktop','')
                            NEIC[f].append('exec_:' + l.split('=')[1].replace(
                            '%U', '').replace('%u', '').replace(
                            '%F', '').replace('%f', '').strip())
                            break
                    except:
                        pass
        # Result is {chromium:['name:Chromium', 'exec_:chromium']}

        # The most difficult part: gathering the icons
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():    
                    try:
                        if l.startswith('Icon='):
                            f = f.replace('.desktop','')
                            NEIC[f].append('icon:' + l.split('=')[1].strip())
                            break
                    except: 
                        pass


        if 'Icons' not in os.listdir(os.path.join(self.cwd, 'Images')):
            icons2 = dict()
            os.mkdir(os.path.join(self.cwd, 'Images', 'Icons'), 0o755)
            for value in NEIC.values():
                try:
                    shutil.copy(icons[value[2].replace('icon:', '')], os.path.join(self.cwd, 'Images', 'Icons'))
                except:
                    shutil.copy(os.path.join(self.cwd, 'Images', 'applications-system.png'), os.path.join(self.cwd, 'Images', 'Icons'))
 
            for value in NEIC.values():
                try:
                    icons2[value[0].replace('name:', '')] = icons[value[2].replace('icon:', '')]     
                except:
                    icons2[value[0].replace('name:', '')] = os.path.join(self.cwd, 'Images', 'applications-system.png')        

            with open(os.path.join(self.cwd, 'dictionary_names_icons'), 'w') as wfile:
                for key, value in icons2.items():
                    wfile.write('{}:{}\n'.format(key, value))

        # Category is extracted
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Categories='):
                            for c in categories:
                                if c[0] in l:
                                    f = f.replace('.desktop','')
                                    NEIC[f].append('category:' + c[0])
                                    break
                    except:
                        pass

        # Final output:
        # {chromium:['name:..., 'exec_:..., 'icon:..., category:...]}
        # From now on, thanks to keys and values which are in both dictionaries
        # NEIC and dictIcons), it is possible to replace 
        # the name of each icon by its absolute path.

        rejected_items = [] # these variables delete duplicated applications
        rejected_values = []

        for key, value in NEIC.items():
            try:
                self.application_icon = Image.open(
                    icons[value[2].replace('icon:', '')])
                self.application_icon.thumbnail((24, 24))
                self.application_icon = ImageTk.PhotoImage(
                    self.application_icon, master = self.root)
                value[2:3] = [self.application_icon]
            except:
                #Default icon if missing icon
                self.application_icon = Image.open(os.path.join(
                    self.cwd,
                    'Images',
                    'applications-system.png'))
                self.application_icon.thumbnail((24, 24))
                self.application_icon = ImageTk.PhotoImage(
                    self.application_icon, master = self.root)
                value[2:3] = [self.application_icon]

            if len(value) != 4:
                rejected_items.append(key)

            if value[0] not in rejected_values:
                rejected_values.append(value[0])
            else:
                rejected_items.append(key)

        for key in rejected_items:
            del NEIC[key] # Duplicated applications are deleted
            
        NEI_by_categories = [] # Applications are sorted by categories

        for c in categories:
            category = [c[0]]
            for key, value in NEIC.items():
                if c[0] == value[3][len('category:'):]:
                    category.append(key)
            NEI_by_categories.append(category)
      
      
        # FILE MANAGER:
        # Let's determine the file manager by default. We need this information
        # to open the first submenu called 'Applications'
        fileManager = ''
        for application in os.listdir(applications):
            if os.path.isfile(os.path.join(applications, application)):
                with open(os.path.join(applications, application),'r') as rfile:
                    for l in rfile.readlines():
                        if 'FileManager' in l and 'Categories=' in l:
                            fileManager = application
                            break

        # When a file manager is found, it is easy to get its command
        with open(os.path.join(applications, fileManager), 'r') as rfile:
            for l in rfile.readlines():
                if l.startswith('Exec='):
                    self.fileManager_exec = l.split('=')[1].strip().replace('%U',
                    '').replace('%u', '').replace('%F', '').replace('%f', '')
                    break

        # TERMINAL:
        # necessary for some applications like 'gparted' for example
        terminal = ''
        for application in os.listdir(applications):
            if os.path.isfile(os.path.join(applications, application)):
                with open(os.path.join(applications, application),'r') as rfile:
                    for l in rfile.readlines():
                        if 'TerminalEmulator' in l:
                            terminal = application
                            break

        # getting the command of the selected terminal
        with open(os.path.join(applications, terminal), 'r') as rfile:
            for l in rfile.readlines():
                if l.startswith('Exec='):
                    #command
                    self.startTerminal = l.split('=')[1].strip() 
                    break

        # Icon images of the ten categories are instantiated with PIL
        for i, category in enumerate(categories):
            self.categoryIcon = Image.open(os.path.join(self.cwd, 'Images',
                category[2]))
            self.categoryIcon.thumbnail((25, 25))
            self.categoryIcon = ImageTk.PhotoImage(self.categoryIcon,
                master=self.menuToplevel)
            categories[i][2:3] = [self.categoryIcon]

            self.categoryButton = tk.Button(
                self.menuLabelFrame,
                bg = 'black',
                compound = 'left',
                text = categories[i][1],
                bd = 0,
                anchor = 'w',
                image = categories[i][2],
                foreground = 'white',
                cursor = 'hand2',
                highlightthickness = 0,
                activebackground = 'navajowhite',
                activeforeground = 'black',
                font = 'Courier 11 bold',
                justify = 'center')
            self.categoryButton.grid(row=i, sticky='news')

            init = dict()
            init = {
                'cwd': self.cwd,
                'root': self.root,
                'category': category,
                'menuButton': self.menuButton,
                'menuToplevel': self.menuToplevel,
                'menuLabelFrame': self.menuLabelFrame,
                'NEI_by_categories': NEI_by_categories,
                'wallpaperToplevel': self.wallpaperToplevel,
                'screenXY':self.screenXY,
                'screenWidth':self.screenWidth,
                'screenHeight':self.screenHeight,
                'NEIC': NEIC,
                'categoryButton': self.categoryButton,
                'i': i
                }

            self.subMenu = SubMenu(init)

            if i==0:
                self.categoryButton.config(
                    command = lambda arg = self.categoryButton:\
                        self.firstCategory(arg))
                self.categoryButton.bind('<Enter>', self.destroySubmenus)
            elif i==1:
                self.categoryButton.config(
                    command = lambda arg=self.categoryButton:\
                        self.secondCategory(arg))
                self.categoryButton.bind('<Enter>', self.destroySubmenus)
            else:
                self.categoryButton.bind('<Enter>', self.subMenu.subMenu)

        init = dict()
        init = {
            'root': self.root,
            'cwd': self.cwd,
            'screenWidth': self.screenWidth,
            'screenHeight': self.screenHeight
            }

        self.disconnect = Disconnect(init)
        self.disconnect.disconnect()

#---- METHOD -------------------------------------------------------------------

    def withdrawMenu(self, event=None):
        "withdraws the menu and toggles the command of the menu button"

        self.menuToplevel.withdraw()
        self.destroySubmenus()
        self.menuButton.config(command = self.deiconifyMenu)

#---- METHOD -------------------------------------------------------------------

    def deiconifyMenu(self, event=None):
        "deiconifies the menu and toggles the command of the menu button"

    def deiconifyMenu(self, event=None):
        "deiconifies the menu and toggles the command of the menu button"

        self.destroySubmenus()
        for child in self.root.winfo_children():
            if child.winfo_class() == 'Toplevel' and not self.screenWidth in\
                child.winfo_geometry():
                child.withdraw()
        if event == None:
            MainMenu.floatingMenu = False
            self.menuToplevel.geometry('+{}-30'.format(self.screenXY.replace(
                '+', ' ').replace('-', ' ').split()[0]))
        elif not event == None:
            MainMenu.floatingMenu = True
            self.menuToplevel.geometry('+{}-{}'.format(self.wallpaperToplevel.\
                winfo_pointerx(), int(self.screenHeight) - int(
                self.wallpaperToplevel.winfo_pointery())))
        self.menuToplevel.update_idletasks()
        self.menuToplevel.deiconify()

        #Correction of the widget position due to visibility issues
        if int(self.menuToplevel.winfo_y()) < 0: # correction in y
            self.menuToplevel.geometry('+{}+0'.format(self.wallpaperToplevel.\
                winfo_pointerx()))

        self.menuButton.config(command=self.withdrawMenu)

#---- METHOD -------------------------------------------------------------------

    def destroySubmenus(self, event = None):
        "This short method destroys the submenus"

        for child in self.menuToplevel.winfo_children():
            if child.winfo_class() == 'Toplevel':
                child.destroy()
                break

#---- METHOD -------------------------------------------------------------------

    def firstCategory(self, firstButton = None):
        "Opens /usr/share/applications"

        self.destroySubmenus()

        self.PID = subprocess.Popen([self.fileManager_exec.strip(),\
            '/usr/share/applications'], stdout = subprocess.PIPE,\
            stderr = subprocess.STDOUT).pid

        self.menuToplevel.withdraw()
        self.menuButton.config(command = self.deiconifyMenu)

#---- METHOD -------------------------------------------------------------------

    def secondCategory(self, secondButton = None):
        "Opens /home/<user>"

        self.destroySubmenus()

        self.PID = subprocess.Popen([self.fileManager_exec.strip(),\
            '/home/{}'.format(os.environ['USER'])], stdout=subprocess.PIPE,\
            stderr=subprocess.STDOUT).pid

        self.menuToplevel.withdraw()
        self.menuButton.config(command=self.deiconifyMenu)

#==== CLASS ====================================================================

class SubMenu(object):
    "This class instantiates the submenus containing all the applications" 

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"    

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def subMenu(self, event) :
        "instantiates the submenus"

        init = dict()
        init = {
            'cwd': self.cwd,
            'root': self.root,
            'category': self.category,
            'menuButton': self.menuButton,
            'menuToplevel': self.menuToplevel,
            'menuLabelFrame': self.menuLabelFrame,
            'wallpaperToplevel': self.wallpaperToplevel,
            'screenXY':self.screenXY,
            'screenWidth':self.screenWidth,
            'screenHeight':self.screenHeight
            }

        self.destroySubmenus = MainMenu(init)
        self.destroySubmenus.destroySubmenus()

        # Submenu of the selected category
        self.submenuToplevel = tk.Toplevel(self.menuToplevel, bg='black')
        self.submenuToplevel.title('Applications')
        self.submenuToplevel.overrideredirect(1)
        self.submenuToplevel.withdraw() 
        id_ = self.categoryButton.after(500, self.submenuToplevel.deiconify)
        self.categoryButton.bind('<Leave>', lambda event :self.categoryButton.\
            after_cancel(id_))
        self.subLabelFrame = tk.LabelFrame(self.submenuToplevel, bg='black',
            fg='navajowhite', text = self.category[1] + ' ', bd = 2,
            font='Courier 11 bold')
        self.subLabelFrame.pack(padx=10, pady=10)

        #self.id_2 = id(self.subLabelFrame.bind(
         #   '<Leave>', self.destroy_main_and_submenu)))
        #self.menuToplevel.bind('<Enter>', lambda event,
         #   arg=self.id_2:self.menuToplevel.after_cancel(arg))

        for c in self.NEI_by_categories:
            if c[0] == self.category[0]:
                for i, key in enumerate(c[1:]):
                    self.buttonInSubmenu = tk.Button(
                        self.subLabelFrame,
                        text = self.NEIC[key][0].replace('name:', ''),
                        image = self.NEIC[key][2],
                        compound = 'left',
                        background = 'black',
                        foreground = 'white',
                        font = 'Courier 11 bold',
                        anchor = 'w',
                        justify = 'center',
                        bd = 0,
                        highlightthickness = 0,
                        activebackground = 'navajowhite',
                        activeforeground = 'black',
                        cursor = 'hand2')
                    self.buttonInSubmenu.grid(row = i, sticky='news')
                    self.buttonInSubmenu.config(
                        command=lambda arg = self.NEIC[key][1].replace(
                        'exec_:',''), arg2 = self.buttonInSubmenu:self\
                        .execAndDestroyMainMenu(arg, arg2))
        self.menuToplevelX = int(self.menuToplevel.winfo_geometry().replace(
            'x', ' ').replace('+', ' ').replace('-', ' ').split()[2])     
        self.menuToplevelWidth = int(self.menuToplevel.winfo_geometry().replace(
            'x', ' ').replace('+', ' ').replace('-', ' ').split()[0]) 

        # The following part is very important. The floating menu is placed and 
        # adjusted in case it is to close to the edges of the screen.
        self.submenuToplevel.update_idletasks()
        self.submenuToplevel.geometry('+{}-{}'.format(self.menuToplevelX +\
            self.menuToplevelWidth, int(self.menuToplevel.winfo_screenheight())\
            - int(self.categoryButton.winfo_rooty()) - int(self.categoryButton.\
            winfo_height())-22))
        self.submenuToplevel.update_idletasks()
        self.i = 2

        topLevelHeight = int(
            self.submenuToplevel.winfo_geometry().split('x')[1].split('+')[0])

        if topLevelHeight >= int(self.root.winfo_screenheight()):
            self.buttonLift = tk.Button(
                self.subLabelFrame,
                text = '\u25B2',
                background = 'black',
                foreground = 'white',
                font = 'Courier 16 bold',
                bd = 0,
                highlightthickness = 0,
                activebackground = 'navajowhite',
                activeforeground = 'black',
                cursor = 'hand2',
                repeatdelay = 500,
                repeatinterval = 100,
                command = self.down)
            self.buttonLift.grid(row = 0, sticky='news')

            self.buttonLower = tk.Button(
                self.subLabelFrame,
                text = '\u25BC',
                background ='black',
                foreground ='white',
                font ='Courier 16 bold',
                bd = 0,
                highlightthickness = 0,
                activebackground = 'navajowhite',
                activeforeground = 'black',
                cursor = 'hand2',
                repeatdelay = 500,
                repeatinterval = 100,
                command = self.up)
            self.buttonLower.grid(row = 1, sticky='news')

        # adjusted in y
        if int(self.submenuToplevel.winfo_y()) < 0:
            self.submenuToplevel.geometry('+{}+0'.format(self.menuToplevelX +\
            self.menuToplevelWidth))
        self.submenuToplevel.update_idletasks()

        if self.subLabelFrame.winfo_children() == []:
            self.categoryButton.grid_forget()

#---- METHOD -------------------------------------------------------------------

    def destroy_main_and_submenu(self, event = None):

        self.destroySubmenus.destroySubmenus()
        self.destroySubmenus.menuToplevel.withdraw()
        self.menuButton.config(command=self.destroySubmenus.deiconifyMenu)

#---- METHOD -------------------------------------------------------------------

    def up(self):
        "reduces height of submenu if this one is more than screen height"

        self.subLabelFrame.winfo_children()[self.i].grid_forget()
        if self.i >= 2 and self.i < 20:
            self.i += 1

#---- METHOD -------------------------------------------------------------------

    def down(self):
        "increases height of submenu if this one has been reduced"

        if self.i > 2:
            self.i -= 1
        self.subLabelFrame.winfo_children()[self.i].grid(
            row = self.i,
            sticky='news')

#---- METHOD -------------------------------------------------------------------

    def execAndDestroyMainMenu(self, exec_, buttonInSubmenu):
        "Execution of the selected application"

        self.exec_ = exec_
        self.buttonInSubmenu = buttonInSubmenu
        self.PID = subprocess.Popen(self.exec_.split(), stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT).pid

        self.submenuToplevel.withdraw()
        self.menuToplevel.withdraw()
        init2 = dict()
        init2 = {
            'root': self.root,
            'category': self.category,
            'wallpaperToplevel': self.wallpaperToplevel,
            'menuToplevel': self.menuToplevel,
            'menuToplevelWidth': self.menuToplevelWidth,
            'menuButton': self.menuButton,
            'cwd': self.cwd,
            'screenXY':self.screenXY,
            'screenWidth':self.screenWidth,
            'screenHeight':self.screenHeight
            }
        self.deiconifyMenu = MainMenu(init2)
        self.menuButton.config(command=self.deiconifyMenu.deiconifyMenu)

#==== CLASS ====================================================================

class Disconnect(object):
    "instantiates an icon in the taskbar for each running application"

    disconnect_toplevels = []

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def disconnect(self):

#---- List of lists for the images. We use PIL ---------------------------------

        self.icons = list()
        imgs = [
                ['suspend.png', (24, 24)],
                ['logout.png', (24, 24)],
                ['reboot.png', (24, 24)],
                ['shutdown.png', (24, 24)]
               ]

        for im in imgs:
            self.icon = Image.open(os.path.join(self.cwd, 'Images', im[0]))
            self.icon.thumbnail(im[1])
            self.icon = ImageTk.PhotoImage(self.icon, master=self.root)
            self.icons.append(self.icon) 

#---- Disconnection (sleep, logout, reboot, shutdown) --------------------------

        self.disconnectFrame = tk.Frame(
            self.root,
            bg = 'black',
            bd = 1,
            highlightbackground = 'navajowhite',
            highlightthickness = 1)
        self.disconnectFrame.pack(side = 'left')

        functions = {
            self.icons[0]:(self.suspend, 'mettre en veille?'),
            self.icons[1]: ('openbox --exit', 'fermer la session?'), 
            self.icons[2]: ('systemctl reboot', 'redémarrer?'),
            self.icons[3]:('systemctl poweroff', 'éteindre?')
                    }

        for k, v in functions.items():
            button = tk.Button(
                self.disconnectFrame,
                bg = 'black',
                bd = 0,
                cursor = 'hand2',
                highlightthickness = 0,
                image = k,
                command = lambda a = k, b = v[0], c = v[1] : self.conf(a, b, c))
            button.pack(side='right')
            button.image = k
            if k == self.icons[0]:
                button.config(command = v[0])

        self.root.mainloop()

#--METHOD-----------------------------------------------------------------------

    def conf(self, icon, command, message):
        "A confirmation that everything is OK before shutdown or reboot"
    
        self.icon = icon
        self.command = command
        self.message = message
        
        if len(Disconnect.disconnect_toplevels) > 0:
            Disconnect.disconnect_toplevels[0].destroy() 
            Disconnect.disconnect_toplevels = []

        self.disconnect_toplevel = tk.Toplevel(self.root)
        self.disconnect_toplevel['bg'] = 'black'
        self.disconnect_toplevel.title(self.message)
        #self.toplevel.overrideredirect(0)

        self.toplevelFrame = tk.Frame(
            self.disconnect_toplevel,
            bg = 'white')
        self.toplevelFrame.grid(padx = 3, pady = 3)
        self.disconnect_toplevel.geometry(
            '-{}-{}'.format(
                int(int(self.screenWidth)/2),
                int(int(self.screenHeight)/2)))
        
        tk.Label(
            self.toplevelFrame,
            image = self.icon,
            compound = 'left',
            text = '  Voulez-vous vraiment\n{}'.format(self.message),
            bg = 'white',
            fg = 'black',
            font = 'Courier 12 bold',
            anchor = 'e').grid(
                padx = 10,
                pady=10,
                columnspan=2)

        tk.Button(
            self.toplevelFrame,
            text = 'oui',
            command = lambda : os.system(self.command),
            bg = 'green',
            fg = 'white',
            font = 'Courier 12 bold',
            cursor = 'hand2',
            activebackground = 'black',
            activeforeground = 'white').grid(
                row = 1,
                column = 0,
                padx = 10,
                pady = 5)

        tk.Button(
            self.toplevelFrame,
            text = 'non',
            bg = 'red',
            fg = 'white',
            font = 'Courier 12 bold',
            command = lambda : self.disconnect_toplevel.withdraw(),
            activebackground = 'black',
            cursor = 'hand2',
            activeforeground = 'white').grid(
                row = 1,
                column = 1,
                padx = 10,
                pady = 5)

        Disconnect.disconnect_toplevels.append(self.disconnect_toplevel)

#--METHOD-----------------------------------------------------------------------

    def suspend(self):
        "suspends the computer"

        subprocess.Popen(
            ['systemctl', 'suspend'],
            text = True,
            stdout=subprocess.PIPE)


# ==== MAIN PROGRAMM ===========================================================

if __name__ == '__main__':
    "This file launches the main menu, the submenus and the wallpaper(s)"

    init = {
        'cwd': os.path.abspath(os.path.dirname(sys.argv[0])),
        'screenWidth': sys.argv[1],
        'screenHeight': sys.argv[2],
        'screenXY': sys.argv[3],
        'wallpaper': sys.argv[4]
        }

    mainMenu = MainMenu(init)
    mainMenu.mainMenu() # It works like a charm
