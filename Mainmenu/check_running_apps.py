#!/usr/bin/env python3
# -*- coding: utf8 -*-

#Copyright © 2020 Benoît Boudaud <http://miamondo.org>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>

#==== ABOUT THIS SCRIPT ========================================================

"""The program instantiates an icon in the taskbar for each running application.
It is part of a personal project named 'Labortablo'. This project aims to create 
a desktop environment entirely written in Python and using the GUI Tkinter."""

#==== PREREQUISITES ============================================================

# see https://gitlab.com/miamondo/labortablo/-/blob/master/README.md

#======= MODULES ===============================================================

import os
import sys
import time
import multiprocessing
import subprocess
import hashlib
import tkinter as tk
from PIL import Image, ImageTk

#==== CLASS ====================================================================

class RunningApps(multiprocessing.Process):
    "instantiates an icon in the taskbar for each running application"

    buttonToplevels = list()

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"

        multiprocessing.Process.__init__(self)
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def run(self):
        "gathers all the icons in /usr/share"

        self.root = tk.Tk()
        self.root['bg'] = 'black'
        self.root.overrideredirect(1)
        self.root.geometry(
            '{}x31+{}-{}'.format(int(self.screenWidth) - 139,
                int(self.screenXY.replace('+', ' ').replace('-',
                ' ').split()[0]) + 139, self.screenXY.replace('+',
                ' ').replace('-', ' ').split()[1]))


        # All the png icons in /usr/share are gathered here, in this dictionary:
        icons = dict()
        # minimum size is 48x48 (for a better resolution)
        for root, dirs, files in os.walk('/usr/share'):
            for f in files:
                try:
                    if os.path.splitext(f)[1]==('.png'): # only PNG files
                        img = Image.open(os.path.join(root, f))
                        if img.width >= 48 and img.height >= 48: 
                            icons[os.path.splitext(f)[0]] = os.path.join(root, f)
                except:
                    pass
        
#       The menu is divided in 12 categories
        categories = [
            ['Applications', ' Applications', 'computer-laptop.png'],
            ['Directories', ' Gestionnaire de fichiers', 'thunar.png'],
            ['WebBrowser', ' Internet', 'applications-internet.png'],
            ['Utility', ' Accessoires', 'applications-accessories.png'],
            ['Office', ' Bureautique', 'applications-office.png'],
            ['Settings', ' Réglages', 'preferences-desktop.png'],
            ['Development', ' Developpement', 'applications-development.png'],
            ['Education', ' Éducation', 'applications-education.png'],
            ['Graphics', ' Graphisme', 'applications-graphics.png'],
            ['AudioVideo', ' Multimédia', 'applications-multimedia.png'],
            ['Network', ' Réseau', 'network-wired.png'],
            ['System', ' Système', 'applications-system.png']]

        # In the following dictionary, the program gathers the name, the 
        # command, the icon and the category of each application installed on 
        #the computer.
        # This part is quite difficult to understand. It starts with the name
        self.NEIC = dict() # self.NEIC means Names, Execs, Icons, Categories

        applications = '/usr/share/applications'

        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Name='):
                            f = f.replace('.desktop','')
                            self.NEIC[f] = ['name:' + l.split('=')[1].strip()]
                            break #Result is {chromium:['name:Chromium']}
                    except:
                        pass

        #Let's continue with the command (exec)
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Exec='):
                            f = f.replace('.desktop','')
                            self.NEIC[f].append('exec_:' + l.split('=')[1]\
                                .replace('%U', '').replace('%u', '').replace(
                                '%F', '').replace('%f', '').strip())
                            break
                    except:
                        pass
        # Result is {chromium:['name:Chromium', 'exec_:chromium']}

        # The most difficult part: gathering the icons
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():    
                    try:
                        if l.startswith('Icon='):
                            f = f.replace('.desktop','')
                            self.NEIC[f].append(
                                'icon:' + l.split('=')[1].strip())
                            break
                    except: 
                        pass

        # Category is extracted
        for f in os.listdir(applications):
            with open(os.path.join(applications, f), 'r') as rfile:
                for l in rfile.readlines():
                    try:
                        if l.startswith('Categories='):
                            for c in categories:
                                if c[0] in l:
                                    f = f.replace('.desktop','')
                                    self.NEIC[f].append('category:' + c[0])
                                    break
                    except:
                        pass

        # Final output:
        # {chromium:['name:..., 'exec_:..., 'icon:..., category:...]}
        # From now on, thanks to keys and values which are in both dictionaries
        # self.NEIC and dictIcons), it is possible to replace 
        # the name of each icon by its absolute path.

        rejected_items = [] # these variables delete duplicated applications
        rejected_values = []

        for key, value in self.NEIC.items():
            try:
                self.application_icon = Image.open(
                    icons[value[2].replace('icon:', '')])
                self.application_icon.thumbnail((18, 18))
                self.application_icon = ImageTk.PhotoImage(
                    self.application_icon, master = self.root)
                value[2:3] = [self.application_icon]
            except:
                #Default icon if missing icon
                self.application_icon = Image.open(os.path.join(
                    self.cwd,
                    'Images',
                    'applications-system.png'))
                self.application_icon.thumbnail((18, 18))
                self.application_icon = ImageTk.PhotoImage(
                    self.application_icon, master = self.root)
                value[2:3] = [self.application_icon]

            if len(value) != 4:
                rejected_items.append(key)

            if value[0] not in rejected_values:
                rejected_values.append(value[0])
            else:
                rejected_items.append(key)

        for key in rejected_items:
            del self.NEIC[key] # Duplicated applications are deleted
            
        NEI_by_categories = [] # Applications are sorted by categories

        for c in categories:
            category = [c[0]]
            for key, value in self.NEIC.items():
                if c[0] == value[3][len('category:'):]:
                    category.append(key)
            NEI_by_categories.append(category)
      
        # FILE MANAGER:
        # Let's determine the file manager by default. We need this information
        # to open the first submenu called 'Applications'
        fileManager = ''
        for application in os.listdir(applications):
            if os.path.isfile(os.path.join(applications, application)):
                with open(os.path.join(applications, application),'r') as rfile:
                    for l in rfile.readlines():
                        if 'FileManager' in l and 'Categories=' in l:
                            fileManager = application
                            break

        # When a file manager is found, it is easy to get its command
        with open(os.path.join(applications, fileManager), 'r') as rfile:
            for l in rfile.readlines():
                if l.startswith('Exec='):
                    self.fileManager_exec = l.split('=')[1].strip().replace('%U',
                    '').replace('%u', '').replace('%F', '').replace('%f', '')
                    break

        # TERMINAL:
        # necessary for some applications like 'gparted' for example
        terminal = ''
        for application in os.listdir(applications):
            if os.path.isfile(os.path.join(applications, application)):
                with open(os.path.join(applications, application),'r') as rfile:
                    for l in rfile.readlines():
                        if 'TerminalEmulator' in l:
                            terminal = application
                            break

        # getting the command of the selected terminal
        with open(os.path.join(applications, terminal), 'r') as rfile:
            for l in rfile.readlines():
                if l.startswith('Exec='):
                    #command
                    self.startTerminal = l.split('=')[1].strip() 
                    break

        self.runningApps()

    def runningApps(self):

        i = 0
        try:
            self.b
        except:
            self.b = 0
        while i<2: 
            self.root.update()
            hasher = hashlib.md5()
            with open(
                os.path.join(self.cwd, 'wmctrl.txt'),
                'rb') as rbfile:
                buf = rbfile.read()
                hasher.update(buf)
                if hasher.hexdigest()!=self.b:
                    self.b=hasher.hexdigest()
                    self.updateApps()
            i += 1
            self.root.after(400)
            if i==1:
                i=0

    def updateApps(self):

        for child in self.root.winfo_children():
            child.destroy()

        self.missing_icon = Image.open(
            os.path.join(self.cwd, 'Images', 'applications-system.png'))
        self.missing_icon.thumbnail((18, 18))
        self.missing_icon = ImageTk.PhotoImage(
            self.missing_icon, master = self.root)

        self.thunar_icon = Image.open(
            os.path.join(self.cwd, 'Images', 'thunar.png'))
        self.thunar_icon.thumbnail((18, 18))
        self.thunar_icon = ImageTk.PhotoImage(
            self.thunar_icon, master = self.root)

        self.names = dict()
        self.parameters = dict()
        with open(
            os.path.join(self.cwd, 'wmctrl.txt'), 'r') as rfile:
            wmctrl = rfile.readlines()
            for line in wmctrl:
                for key, value in self.NEIC.items():
                    if value[0].replace('name:', '').lower() in line.lower():
                        self.names[line.split(':')[0]] = [value[0].replace(
                            'name:', ''), value[2]]

        for line in wmctrl:
            if "Gestionnaire de fichiers" in line.strip().split(':')[2]:
                self.names[line.split(':')[0]] = [line.strip().split(':')[2],
                    self.thunar_icon]
            elif line.split(':')[0] not in self.names.keys():
                self.names[line.split(':')[0]] = [line.strip().split(':')[2],
                    self.missing_icon]
        for child in self.root.winfo_children():
            child.destroy()

        self.taskbar_buttons = list()
        for i, (key, val) in enumerate(self.names.items()):
            self.taskbar_button = tk.Menubutton(
                self.root,
                justify = 'center',
                font = 'Courier 10 bold',
                text = val[0],
                image = val[1],
                compound = 'left',
                anchor = 'w',
                bg = 'black',
                fg = 'white',
                relief = 'flat',
                bd = 1,
                highlightbackground = 'navajowhite',
                highlightthickness = 1,
                cursor = 'hand2',
                activeforeground = 'black',
                activebackground = 'navajowhite',
                direction = 'above')
            self.taskbar_button.grid(row = 0, column = i + 1)
            self.taskbar_buttons.append(self.taskbar_button)
            self.taskbar_buttons[i].update_idletasks()
            self.taskbar_buttons[i].bind(
                '<Button-1>',
                lambda event,
                arg = key,
                arg2 = self.taskbar_buttons[i]: self.iconify_app(arg, arg2))
            self.taskbar_buttons[i].bind(
                '<Button-3>',
                lambda event,
                arg = key,
                arg2 = self.taskbar_buttons[i]:self.buttonMenu(arg, arg2))

        self.runningApps()

    def iconify_app(self, IDNumb, taskbar_button):

        self.IDNumb = IDNumb
        self.taskbar_button = taskbar_button

        if len(RunningApps.buttonToplevels) > 0:
            for element in RunningApps.buttonToplevels:
                element.destroy()
                self.hideButtonToplevel(self.IDNumb, self.taskbar_button)

        subprocess.Popen(
            ['xdotool', 'windowminimize', self.IDNumb],
            text = True,
            stdout = subprocess.PIPE)

        self.taskbar_button.bind(
            '<Button-1>',
            lambda event,
            arg=self.IDNumb,
            arg2=self.taskbar_button:self.deiconify_app(arg, arg2))

    def deiconify_app(self, IDNumb, taskbar_button):

        self.IDNumb = IDNumb
        self.taskbar_button = taskbar_button

        if len(RunningApps.buttonToplevels) > 0:
            for element in RunningApps.buttonToplevels:
                element.destroy()
                self.hideButtonToplevel(self.IDNumb, self.taskbar_button)

        subprocess.run(['xdotool', 'windowactivate', self.IDNumb])
        self.taskbar_button.bind(
            '<Button-1>',
            lambda event,
            arg = self.IDNumb,
            arg2 = self.taskbar_button:self.iconify_app(arg, arg2))

    def buttonMenu(self, IDNumb, taskbar_button):
        "opens a little menu above the button"

        self.IDNumb = IDNumb
        self.taskbar_button = taskbar_button

        self.taskbar_icons = list()

        if len(RunningApps.buttonToplevels) > 0:
            for element in RunningApps.buttonToplevels:
                element.destroy()
                self.hideButtonToplevel(self.IDNumb, self.taskbar_button)
       
        self.buttonToplevel = tk.Toplevel(
            self.root,
            bg = 'navajowhite',
            bd = 1)
        self.buttonToplevel.geometry(
            '+{}-30'.format(self.taskbar_button.winfo_rootx()))
        self.buttonToplevel.overrideredirect(1)
        RunningApps.buttonToplevels.append(self.buttonToplevel)

        init = dict()
        init = {
            'cwd': self.cwd,
            'IDNumb': self.IDNumb,
            'taskbar_button': self.taskbar_button,
            'root': self.root,
            'NEIC': self.NEIC,
            'screenXY': self.screenXY,
            'screenWidth': self.screenWidth,
            'buttonToplevel': self.buttonToplevel
            }

        self.favorites = Favorites(init)

        self.PlusMinusClose = [
            ['list-remove.png', 'Supprimer des favoris', lambda: self.remove_from_favorites(self.IDNumb, self.taskbar_button)],
            ['list-add.png', 'Ajouter aux favoris', self.favorites.add_to_favorites],
            ['close.png', 'Fermer l\'application', self.destroyButton]]

        for i, element in enumerate(self.PlusMinusClose):
            self.icon = Image.open(os.path.join(self.cwd, 'Images',element[0]))
            self.icon.thumbnail((16, 16))
            self.icon = ImageTk.PhotoImage(self.icon,
                master=self.root)
            self.taskbar_icons.append(self.icon)

        for i, icon in enumerate(self.PlusMinusClose):
            self.button = tk.Button(
                self.buttonToplevel,
                bg = 'black',
                fg = 'white',
                image = self.taskbar_icons[i],
                compound = 'left',
                anchor = 'w',
                font = 'Courier 10 bold',
                highlightthickness = 0,
                bd = 0,
                justify = 'left',
                text = self.PlusMinusClose[i][1],
                command = self.PlusMinusClose[i][2])
            self.button.grid(row = i, column = 0, sticky = 'news')

#---- METHOD -------------------------------------------------------------------

    def destroyButton(self):
        "destroys the button in the taskbar and closes the application"
    
        subprocess.run(['wmctrl', '-i', '-c', self.IDNumb]),

        try:
            self.buttonToplevel.destroy()
            self.dictButtonPID[self.IDNumb][1].destroy()
        except:
            "Application has already been destroyed"

#---- METHOD -------------------------------------------------------------------

    def hideButtonToplevel(self, IDNumb, taskbar_button):
        "hides the little menu and changes the command of the button"

        self.IDNumb = IDNumb
        self.taskbar_button = taskbar_button

        self.buttonToplevel.destroy()
        self.taskbar_button.bind(
            '<Button-3>',
            lambda event,
            arg = self.IDNumb,
            arg2 = self.taskbar_button: self.buttonMenu(arg, arg2))

#==== CLASS ====================================================================

class Favorites(multiprocessing.Process):
    "instantiates an icon in the taskbar for each running application"

    root = list()

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"

        multiprocessing.Process.__init__(self)
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def run(self):
        "just"


    def add_to_favorites(self):
        "add the running application to favorites"

        if len(Favorites.root) > 0:
            self.favorites_root = Favorites.root[-1]
        else:
            self.favorites_root = tk.Tk()
            self.favorites_root['bg'] = 'green'
            self.favorites_root.overrideredirect(1)
            Favorites.root[0:1] = [self.favorites_root]
            self.favorites_root.geometry(
                '+{}-{}'.format(int(self.screenXY.replace('+', ' ').replace('-',
                ' ').split()[0]) + 139, self.screenXY.replace('+',
                ' ').replace('-', ' ').split()[1]))

        dict_names_icons = dict()
        self.icon = ''
        if "Gestionnaire de fichiers" in self.taskbar_button['text']:
            self.name = 'Thunar File Manager'
        else:
            self.name = self.taskbar_button['text']
        with open(os.path.join(self.cwd, 'dictionary_names_icons'), 'r') as rfile:
            for line in rfile.readlines():
                dict_names_icons[line.split(':')[0]] = line.strip().split(':')[1] 
        if self.name == 'Thunar File Manager':
            self.icon = Image.open(os.path.join(self.cwd, 'Images', 'thunar.png'))
        else:
            self.icon = Image.open(dict_names_icons[self.name])
        self.icon.thumbnail((24, 24))
        self.icon = ImageTk.PhotoImage(
            self.icon, master = self.favorites_root)

        
        for value in self.NEIC.values():
            if self.name == value[0].replace('name:', ''):
                self.favorite_button = tk.Button(
                    self.favorites_root,
                    bg = 'black',
                    fg = 'white',
                    image = self.icon,
                    relief = 'flat',
                    bd = 1,
                    highlightbackground = 'navajowhite',
                    highlightthickness = 1,
                    cursor = 'hand2',
                    activeforeground = 'black',
                    activebackground = 'navajowhite')
                self.favorite_button.grid(row = 0, column = len(self.favorites_root.winfo_children()) - 1)

        self.root.geometry(
            '{}x31+{}-{}'.format(int(self.screenWidth) - 139 + len(self.favorites_root.winfo_children()) * 31,
                int(self.screenXY.replace('+', ' ').replace('-',
                ' ').split()[0]) + 139 + len(self.favorites_root.winfo_children()) * 31, self.screenXY.replace('+',
                ' ').replace('-', ' ').split()[1]))
        self.root.update_idletasks()
            
        init = dict()
        init = {
            'cwd': self.cwd,
            'root': self.root,
            'NEIC': self.NEIC,
            'buttonToplevel': self.buttonToplevel,
            'screenWidth': self.screenWidth,
            'screenXY': self.screenXY
            }
        self.updateApps = RunningApps(init)
        self.favorites_root.after(500, self.updateApps.updateApps) 
        self.favorites_root.update()

                    
    def remove_from_favorites(self, IDNumb, taskbar_button):
        "add the running application to favorites"


#==== MAIN PROGRAMM ============================================================

if __name__ == '__main__':
    "instantiates an icon in the taskbar for each running application"

    init = {
        'cwd': os.path.abspath(os.path.dirname(sys.argv[0])),
        'screenWidth': sys.argv[1],
        'screenHeight': sys.argv[2],
        'screenXY': sys.argv[3]
        }
    runningApps = RunningApps(init)
    runningApps.start()

    favorites = Favorites(init)
    favorites.start()

    runningApps.join()
    favorites.join()

