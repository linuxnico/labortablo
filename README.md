## Name of the program : Labortablo

# What does Labortablo means?
- **Labortablo** is an esperanto word which means "Desktop".

# What is Labortablo?
- **Labortablo** is a desktop environment entirely written in Python. It is a program with a pedagogical purpose.
- My own configuration is Archlinux with Openbox. You can chose another distribution **but you need Openbox**.
- If you don't want to install Archlinux, I suggest you to install a minimal iso of Debian or Ubuntu.

- [A short video that will give you a first impression](https://www.youtube.com/watch?v=r9pEkCy_z5A)

# Prerequisites
- Before starting, you need to install:
    - **Openbox**
    - Python3,
    - Tkinter
    - Python-PIL (for the images)
    - wmctrl
    - xrandr
    - xdotool
    - compton
    - Please, install also the oxygen-icons-theme.

# start.py
- The program is launched by **start.py**. The absolute path of this file is placed in **/etc/xdg/openbox/autostart**. But you can choose to place it in **~/.config/openbox/autostart** if you prefer that it only impacts your own user's configuration


# mainmenu.py
- This is the most important file. It instantiates the main menu and the submenus containing all the applications gathered in /usr/share/applications.
- Regarding the wallpapers, I need to code a GUI in order to let the user choose his own.

# check_running_apps.py

- This file instantiates a button in the taskbar for each running application. By clicking on this button, you can iconify or deiconify.
- With a right click, you can close the running application and destroy the button.

