#!/usr/bin/env python3
# -*- coding: utf8 -*-

#Copyright © 2020 Benoît Boudaud <http://miamondo.org>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>

#==== ABOUT THIS SCRIPT ========================================================

"""This programm launches a desktop environment entirely written in Python."""

#==== PREREQUISITES ============================================================

# see https://gitlab.com/miamondo/labortablo/-/blob/master/README.md

#====== MODULES ================================================================

import os
import sys
import multiprocessing
import subprocess
import threading
import time

#==== CLASS ====================================================================

class Launch(multiprocessing.Process):
    "Launches the desktop environment"

#---- METHOD -------------------------------------------------------------------

    def __init__(self, init):
        "Constructor"
        
        multiprocessing.Process.__init__(self)
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#---- METHOD -------------------------------------------------------------------

    def run(self):
        "Launches all applications composing the desktop environment"
        
        
        subprocess.run(self.pythonFile) #, text=True, stdout=subprocess.PIPE)

#==== MAIN PROGRAMM ============================================================

if __name__ == '__main__':
    "This file launches all applications composing the desktop environment"

# How many monitors are active and connected?
    process = subprocess.Popen(['xrandr', '--listactivemonitors'], text=True,
        stdout=subprocess.PIPE)

    # Output:
    # Monitors: 2
    # 0: +*HDMI-1 1920/698x1080/392+1920+0  HDMI-1
    # 1: +VGA-1 1920/477x1080/268+0+0  VGA-1

    # number of screens and their resolution
    numbOfThreads = int() # matches the number of active monitors
    resolution, screenWidths, screenHeights, screenXYs = [], [], [], []

    for i, line in enumerate(process.stdout.readlines()): 
        if i==0:
            numbOfThreads = line.split(':')[1].strip()
        elif i>0:
            resolution.append(line.split()[2].replace('/', ' ').replace(
            'x', ' '))
    # resolution = ['1920 698 1080 392+1920+0', '1920 477 1080 268+0+0']
    for r in resolution: 
        screenWidths.append(r.split()[0])  #=> ['1920', '1920']
        screenHeights.append(r.split()[2]) #=> ['1080', '1080']
        try:
            screenXYs.append(r.split()[3][r.split()[3].index('+'):])
        except:
            screenXYs.append(r.split()[3][r.split()[3].index('-'):])
    # Output for screenXYs: ['+1920+0', '+0+0']

    # Here, a small GUI is needed, in order to select wallpapers
    wallpapers = ['rakotzbruecke.png', 'rakotzbruecke.png']
    cwd = os.path.abspath(os.path.dirname(sys.argv[0]))

    launches = []

    launch = Launch(
        {'pythonFile':[os.path.join(cwd, 'Mainmenu/loop.py')]})
    launch.start()
    time.sleep(.2)
    launches.append(launch)

    for i in range(0, int(numbOfThreads)):
        launch = Launch(
            {'pythonFile':[os.path.join(cwd, 'Mainmenu/check_running_apps.py'),
            screenWidths[i],
            screenHeights[i],
            screenXYs[i]]})
        launch.start()
        time.sleep(.2)
        launches.append(launch)

    for i in range(0, int(numbOfThreads)):
        launch = Launch(
            {'pythonFile':[os.path.join(cwd, 'Mainmenu/mainmenu.py'),
            screenWidths[i],
            screenHeights[i],
            screenXYs[i],
            wallpapers[i]]})
        launch.start()
        time.sleep(.2)
        launches.append(launch)

    for launch in launches:
        launch.join()
